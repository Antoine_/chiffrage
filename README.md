# Projet : Chiffrage et déchiffrage de message

Ce programme permet le chiffrement et déchiffrement de messages entrés par l'utilisateur.

Pour cela 2 méthodes de chiffrement ont été mises en place :

* le chiffrement César
* le chiffrement Vigenère

## Comportement du programme


Tout d'abord le programme vous demande une phrase à chiffrer puis l'affiche :

```
Quelle est votre phrase ? (déjà chiffrée ou pas)
Je suis un élève sérieux
Votre phrase est : Je suis un élève sérieux
```

Ensuite, le programme enlève les accents et les majuscules de celle-ci :

```
Votre phrase sans accent et sans majuscule : je suis un eleve serieux
```

Pour choisir le chiffrement voulu :

```
Quel chiffrement : 1 pour cesar ou 2 pour vigenere
```
On choisit ensuite si l'on veut chiffrer ou déchiffrer notre phrase : 

```
Entrez 1 pour chiffrer ou 2 pour déchiffrer avec César
```

La phase de chiffrement César se déroule : de la demande de la clé à l'affichage du message chiffré :

Avec les choix `1` et `1` :

```
Quelle clé ? (int attendu)
59

Votre phrase chiffrée : ql zbpz bu lslcl zlyplbe
```

## Utilisation du programme


Il suffit d'entrer la commande `make`

Entrez ensuite la commande `./main` pour éxécuter le programme

La commande `make clean` permet de supprimer le main et les *.o


