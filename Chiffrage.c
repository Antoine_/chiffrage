#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include <wchar.h>
#include <string.h>


// Permet d'enlever les caractères spéciaux 

int verifCar (wchar_t *chaine)
{
	wchar_t carPermitted[]={L" abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖØÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ"};

	int i, j, lgChaine, lgCarPermitted, valeursOk;
	valeursOk = 0;
	
	lgChaine = wcslen(chaine)-1;
	lgCarPermitted = wcslen(carPermitted)-1;

	for  (i=0;i<lgChaine; i++){
		for (j=0;j<lgCarPermitted; j++){
			if (chaine[i]==carPermitted[j]){
				valeursOk++;
			}
		}
	}

	if (valeursOk == lgChaine ){
		return 0;
	}else{
		return 1;
	}
}



//Enlève les accents et les majuscules de la chaine de caractères

void convertirAccentEtMaj (wchar_t *chaine)
{
	
	int i, j, lgChaine, lgList;
	wchar_t listA[]={L"ÀÁÂÃÄÅàáâãäå"};
	wchar_t listE[]={L"ÈÉÊËèéêë"};
	wchar_t listI[]={L"ìíîïÌÍÎÏ"};
	wchar_t listO[]={L"ðòóôõöÒÓÔÕÖ"};
	wchar_t listU[]={L"ùúûüÙÚÛÜ"};
	

	lgChaine = wcslen(chaine)-1;
	lgList = 6;
	

	for  (i=0;i<lgChaine; i++){
		for  (j=0;j<lgList; j++){
			if (chaine[i]==listA[j]){
				chaine[i]='a';
			}
			else if (chaine[i]==L'ç' || chaine[i]==L'Ç'){
				chaine[i]='c';
			}
			else if (chaine[i]==listE[j]){
				chaine[i]='e';
			}
			else if (chaine[i]==listI[j]){
				chaine[i]='i';
			}
			else if (chaine[i]==listO[j]){
				chaine[i]='o';
			}
			else if (chaine[i]==listU[j]){
				chaine[i]='u';
			}
			else if (chaine[i]==L'ý' || chaine[i]==L'ÿ' || chaine[i]==L'Ý'){
				chaine[i]='y';
			}
		}
		if (chaine[i]>=65 && chaine[i]<=90) {
			chaine[i]+=32;
		}
	}
}




// Chiffrement de César

void chiffrementCesar (wchar_t *chaineAChiffrer, int cle)
{
	int lgChaine = wcslen(chaineAChiffrer)-1;
	int i, decalage, premCarASCII, valeurCarASCII, valeurChiffreASCII, ecart;
	
	premCarASCII=97;
	
	for  (i=0;i<lgChaine; i++){
		if (chaineAChiffrer[i]==32){
			chaineAChiffrer[i]==32;
		}else{
			decalage=cle%26;
			valeurCarASCII = (int) chaineAChiffrer[i];
			ecart = valeurCarASCII - premCarASCII;
			valeurChiffreASCII = (ecart + decalage) % 26 + premCarASCII; 
			chaineAChiffrer[i]=valeurChiffreASCII;
		}
	}
}





// Déchiffrement de César

void dechiffrementCesar (wchar_t *chaineADechiffrer, int cle)
{
	int lgChaine = wcslen(chaineADechiffrer)-1;
	int i, decalage, premCarASCII, valeurCarASCII, valeurDechiffreASCII, ecart;
	
	premCarASCII=97;
	
	for  (i=0;i<lgChaine; i++){
		if (chaineADechiffrer[i]==32){
			chaineADechiffrer[i]==32;
		}else{
			decalage=cle%26;
			valeurCarASCII = (int) chaineADechiffrer[i];
			ecart = valeurCarASCII - premCarASCII;
			valeurDechiffreASCII = (ecart - decalage + 26) % 26 + premCarASCII; 
			chaineADechiffrer[i]=valeurDechiffreASCII;
		}
	}
}




//Chiffrement de Vigenère

void chiffrementVigenere (wchar_t *chaineAChiffrer, wchar_t *cle)
{
	int lgChaine = wcslen(chaineAChiffrer)-1;
	int lgCle = wcslen(cle)-1;
	int i, decalage, premCarASCII, valeurCarASCII, valeurChiffreASCII, ecart;
	
	premCarASCII=97;
	
	for  (i=0;i<lgChaine;i++){
		if (chaineAChiffrer[i]==32){
			chaineAChiffrer[i]==32;
		}else{
			decalage=(int) cle[i%lgCle] - 96;
			valeurCarASCII = (int) chaineAChiffrer[i];
			ecart = valeurCarASCII - premCarASCII;
			valeurChiffreASCII = (ecart + decalage) % 26 + premCarASCII; 
			chaineAChiffrer[i]=valeurChiffreASCII;
		}
	}
}



//Déchiffrement de Vigenère

void dechiffrementVigenere (wchar_t *chaineADechiffrer, wchar_t *cle)
{
	int lgChaine = wcslen(chaineADechiffrer)-1;
	int lgCle = wcslen(cle)-1;
	int i, decalage, premCarASCII, valeurCarASCII, valeurDechiffreASCII, ecart;
	
	premCarASCII=97;
	
	for  (i=0;i<lgChaine; i++){
		if (chaineADechiffrer[i]==32){
			chaineADechiffrer[i]==32;
		}else{
			decalage=(int) cle[i%lgCle] - 96;
			valeurCarASCII = (int) chaineADechiffrer[i];
			ecart = valeurCarASCII - premCarASCII;
			valeurDechiffreASCII = (ecart - decalage + 26) % 26 + premCarASCII; 
			chaineADechiffrer[i]=valeurDechiffreASCII;
		}
	}
}





int main(int argc, char *argv[])
{
	setlocale (LC_ALL, "");
	
	wchar_t chaine[200];

	wprintf(L"Quelle est votre phrase ? (déjà chiffrée ou pas) \n");
	fgetws(chaine,200,stdin);
	wprintf(L"Votre phrase est : %ls\n", chaine);


	//Vérif des caractères speciaux
	if (verifCar(chaine)==1){
		wprintf(L"Vous avez mis un caractère non autorisé dans votre phrase \n");
		return 0;
	}
	
	
	//Enleve accent de chaine
	convertirAccentEtMaj(chaine);
	wprintf(L"Votre phrase sans accent et sans majuscule : %ls\n", chaine);



	//Demande choix du chiffrement
	int cesOuVig;
	wprintf(L"Quel chiffrement : 1 pour César ou 2 pour Vigenère\n");
	wscanf(L"%d", &cesOuVig);


	if (cesOuVig==1) {

		//Choix entre chiffré ou déchiffré la phrase
		int chiffrOuDechiffr;
		wprintf(L"Entrez 1 pour chiffrer ou 2 pour déchiffrer avec César\n");
		wscanf(L"%d", &chiffrOuDechiffr);

		if (chiffrOuDechiffr==1) {

			//Chiffrement César

			//Demande clé
			int cle;
			wprintf(L"Quelle clé ? (int attendu) \n");
			wscanf(L"%d", &cle);
			wprintf(L"\n");

			//Chiffre
			wchar_t chaineChiffreCesar[200];
			wcscpy(chaineChiffreCesar,chaine);
			chiffrementCesar(chaineChiffreCesar,cle);
			wprintf(L"Votre phrase chiffrée : %ls\n", chaineChiffreCesar);
		

		}else if (chiffrOuDechiffr==2) {

			//Déchiffrement César

			//Demande clé
			int cle;
			wprintf(L"Quelle est la clé du message chiffré ? (int attendu) \n");
			wscanf(L"%d", &cle);
			wprintf(L"\n");

			//Déchiffre César
			wchar_t chaineDechiffreCesar[200];
			wcscpy(chaineDechiffreCesar,chaine);
			dechiffrementCesar(chaineDechiffreCesar,cle);
			wprintf(L"Votre phrase déchiffrée : %ls\n", chaineDechiffreCesar);
		}

	}else if (cesOuVig==2) {

		int chiffrOuDechiffr;
		wprintf(L"Entrez 1 pour chiffrer ou 2 pour déchiffrer avec Vigenère\n");
		wscanf(L"%d", &chiffrOuDechiffr);

		if (chiffrOuDechiffr==1) {

			//Chiffrement Vigenere

			//Demande clé
			wchar_t clef[200];
			wprintf(L"Quelle est votre clé ? (1 mot attendu : minimum 2 chars) \n");
			wscanf(L"%ls",&clef);
			wprintf(L"Valeur de la clé : %ls\n",clef);

			//Chiffre
			wchar_t chaineChiffreVigenere[200];
			wcscpy(chaineChiffreVigenere,chaine);
			chiffrementVigenere(chaineChiffreVigenere,clef);
			wprintf(L"Votre phrase chiffrée : %ls\n", chaineChiffreVigenere);

		}else if (chiffrOuDechiffr==2) {

			//Déchiffrement Vigenere

			//Demande clé
			wchar_t clef[200];
			wprintf(L"Quelle est la clé de chiffrage ? (1 mot attendu : minimum 2 chars) \n");
			wscanf(L"%ls",&clef);
			wprintf(L"Valeur de la clé : %ls\n",clef);

			//Déchiffre César
			wchar_t chaineDechiffreVigenere[200];
			wcscpy(chaineDechiffreVigenere,chaine);
			dechiffrementVigenere(chaineDechiffreVigenere,clef);
			wprintf(L"Votre phrase déchiffrée : %ls\n", chaineDechiffreVigenere);

		}

	}else{
		wprintf(L"Saisie incorrect, veuillez saisir un choix entre 1 (cesar) et 2 (vigenere)");
	}


	return 0;
}